const { remote } = require('electron');
remote.app.commandLine.appendSwitch('js-flags', '--max-old-space-size=8192');
const path            = require('path');
const es              = require('event-stream');
const fs              = require('fs');
const isDev           = require('electron-is-dev');
const BigMap          = require('../lib/BigMap');


const numberCode = JSON.parse(fs.readFileSync(path.join(__dirname, 'js', 'numberCode.json'), 'utf8'));

// Close window
const closeWindowBtn = document.querySelector('.titleBar__btn--close');
closeWindowBtn.addEventListener('click', () =>
{
	remote.getCurrentWindow().close();
});


// Minimize window
const minimizeWindowBtn = document.querySelector('.titleBar__btn--minimize');
minimizeWindowBtn.addEventListener('click', () =>
{
	remote.getCurrentWindow().minimize()
});

// City select
const citySelect = document.querySelector('.selectCity');
const citySelectWrap = document.querySelector('.selectCity-wrap');

for(let city in numberCode)
{
	citySelect[citySelect.options.length] = new Option(city);
};
const choices = new Choices(citySelect,
	{
		noResultsText: 'Результаты не найдены',
		position: 'bottom'
	});


// Select parsing type
let prasingTypeSelect = 0;
const PARSING_TYPE = ['Нормализатор строк', 'Нормализатор балансов', 'Сортировка по городам'];

const SELECT_TYPE = document.querySelector('.selectType');
const SELECT_TYPE_PREV = SELECT_TYPE.querySelector('.selectType__btn--prev');
const SELECT_TYPE_NEXT = SELECT_TYPE.querySelector('.selectType__btn--next');
const SELECT_TYPE_TEXT = SELECT_TYPE.querySelector('.selectType__text');

SELECT_TYPE_TEXT.innerText = PARSING_TYPE[prasingTypeSelect];
SELECT_TYPE_TEXT.dataset.typeSelect = prasingTypeSelect;


SELECT_TYPE_PREV.addEventListener('click', () =>
{
	prasingTypeSelect--;

	if (prasingTypeSelect < 0)
	{
		prasingTypeSelect = PARSING_TYPE.length - 1;
	}

	if (prasingTypeSelect == 2)
	{
		citySelectWrap.classList.remove('hide');
	}
	else
	{
		citySelectWrap.classList.add('hide');
	}

	SELECT_TYPE_TEXT.innerText = PARSING_TYPE[prasingTypeSelect];
	SELECT_TYPE_TEXT.dataset.typeSelect = prasingTypeSelect;
});

SELECT_TYPE_NEXT.addEventListener('click', () =>
{
	prasingTypeSelect++;
	if (prasingTypeSelect == PARSING_TYPE.length)
	{
		prasingTypeSelect = 0;
	}

	if (prasingTypeSelect == 2)
	{
		citySelectWrap.classList.remove('hide');
	}
	else
	{
		citySelectWrap.classList.add('hide');
	}

	SELECT_TYPE_TEXT.innerText = PARSING_TYPE[prasingTypeSelect];
	SELECT_TYPE_TEXT.dataset.typeSelect = prasingTypeSelect;
});

// Select file
const selectFile = document.querySelector('.selectFile');


// Select file to parsing
const selectFileBtn = document.querySelector('.selectFile__btn');
selectFileBtn.addEventListener('click', () =>
{
	remote.dialog.showOpenDialog(remote.getCurrentWindow(),
	{
		properties : ['openFile', 'showHiddenFiles'],
		title      : 'Выбери файл для парсинга',
		buttonLabel: 'Выбрать',
		filters    :
		[
			{name: 'Text file', extensions: ['txt']},
		]
	})
		.then(data => data.filePaths)
		.then(files =>
		{
			if (!files.length)
			{
				return;
			}

			let type = SELECT_TYPE_TEXT.dataset.typeSelect;
			let file = files[0].split(String.fromCharCode(92)).join(String.fromCharCode(92,92));

			parsingFile(file, type);
		});

}, false);


// Parsing info
const parsingInfo = document.querySelector('.parsingInfo');
const parsingInfoBtn = parsingInfo.querySelector('.parsingInfo__btn');
parsingInfoBtn.addEventListener('click', () =>
{
	parsingInfo.classList.add('hide');
	selectFile.classList.remove('hide');
}, false)


function parsingFile(file, type)
{
	addToLogFile(`Parsing run.`);
	selectFile.classList.add('hide');
	parsingInfoBtn.classList.add('hide');

	// Clear block parsingInfo, except parsingInfo__btn
	while(!parsingInfo.lastElementChild.classList.contains('parsingInfo__btn'))
	{
		parsingInfo.removeChild(parsingInfo.lastElementChild);
	}

	let startParsingTime = new Date().getTime();

	let filePath = path.parse(file);

	parsingInfo.classList.remove('hide');
	let status = addParsingInfo('Статус: ', 'Обработка файла..', 'status');
	addParsingInfo('Файл: ', filePath.base, 'fileName', 'bottom');

	let fileName = `${filePath.name}__parsed${filePath.ext}`;

	if (type == 0)
	{
		var data = new BigMap();
		var count = 0;
		var count_double = 0;
	}
	else if (type == 1)
	{
		var data = [];
		var regex = /^(?<telNumber>\d+)[;:](?<password>.*)\|\s+Balance:\s+(?<sum>\d+);\s+Card:\s+(?<cardNumber>\d+)/;
	}
	else if (type == 2)
	{
		var badNumber = 0;
		var goodNumber = 0;
		var city = choices.getValue(1);
		fileName = `${filePath.name} - ${city}${filePath.ext}`;
	}


	let fileSavePath = path.join(filePath.dir, fileName);


	let stream = fs.createWriteStream(fileSavePath);
		stream.once('open', () =>
		{
			fs.createReadStream(path.format(filePath))
			.pipe(es.split())
			.pipe(es.mapSync(line =>
				{
					if (type == 0)
					{
						line = line.replace(/^\+/, '').replace(/;/g, ':');

						if (!line || /[А-Яа-я]/.test(line) || !/[7-9]/.test(line.substr(0, 1)))
						{
							count++;
							return;
						}


						if (line.substr(0, 1) == 9)
						{
							line = '7' +line;
						}
						else if (line.substr(0, 1) == 8)
						{
							line = '7' +line.substr(1);
						}

						let lineSplit = line.split(':');
						let val = data.get(lineSplit[0]);

						if (data.has(lineSplit[0]) && val.lastIndexOf(lineSplit[1]) !== -1)
						{
							count_double++;
							return;
						}

						data.set(lineSplit[0], (val ? [lineSplit[1], ...val] : [lineSplit[1]]));
						stream.write(`${line}\n`);
					}
					else if (type == 1)
					{
						if (!line)
						{
							return;
						}

						let match = regex.exec(line);

						if (match)
						{
							data.push(Object.assign({}, match.groups));
						}
					}
					else if (type == 2)
					{
						if (!line)
						{
							return;
						}

						line = line.replace(/^\+/, '').replace(/;/g, ':');

						if (!line || /[А-Яа-я]/.test(line) || !/[7-9]/.test(line.substr(0, 1)))
						{
							return;
						}


						if (line.substr(0, 1) == 9)
						{
							line = '7' +line;
						}
						else if (line.substr(0, 1) == 8)
						{
							line = '7' +line.substr(1);
						}

						let lineSplit = line.split(':');

						if (numberIsCity(city, lineSplit[0]))
						{
							goodNumber++;
							stream.write(`${line}\n`);
						}
						else
						{
							badNumber++;
						}
					}
				})
				.on('error', (err) =>
				{
					stream.end();
					status.innerText = 'Ошибка...';
					addParsingInfo('[!] ', err, 'error', 'top');
					addToLogFile(`Ошибка: ${err}`);

					if (data)
					{
						if (type == 0)
						{
							data.clear();
						}
						else if (type == 1)
						{
							delete data;
						}
					}

					parsingInfoBtn.classList.remove('hide');
				})
				.on('end', () =>
				{
					if (type == 1)
					{
						data.sort((a, b) =>
						{
							return b.sum - a.sum
						});

						data.forEach(item =>
						{
							stream.write(`${parseInt(item.sum).toLocaleString('de')} руб - ${item.telNumber}:${item.password} - ${item.cardNumber}\n`);
						});
					}

					stream.end();

					addToLogFile(`Parsing end.`);

					let endParsingTime = new Date().getTime();
					let time = (endParsingTime - startParsingTime) / 1000;

					if (data)
					{
						status.innerText = `Очищаем данные`;
						if (type == 0)
						{
							data.clear();
						}
						else if (type == 1)
						{
							delete data;
						}
					}

					status.innerText = `Завершено`;

					if (type == 0)
					{
						addParsingInfo('Удалено строк: ', count.toLocaleString('de'), 'count');
						addParsingInfo('Удалено дублей: ', count_double.toLocaleString('de'), 'countDouble');
					}
					else if (type == 2)
					{
						addParsingInfo('Нужных номеров: ', goodNumber.toLocaleString('de'), 'goodNumber');
						addParsingInfo('Не нужных номеров: ', badNumber.toLocaleString('de'), 'badNumber');
					}

					addParsingInfo('Прошло времени: ', secondToTime(time), 'time', 'top');

					parsingInfoBtn.classList.remove('hide');

					remote.getCurrentWindow().flashFrame(1);
				})
			);
		});
}

function numberIsCity(city, number)
{
	let numberParsed = /7(?<code>\d{3})(?<range>\d+)/.exec(number).groups;

	let res = false;

	if (numberCode[city].codes[numberParsed.code])
	{
		res = numberCode[city].codes[numberParsed.code].some((item) =>
		{
			if (numberParsed.range >= item[0] && numberParsed.range <= item[1])
			{
				return true;
			}
		})
	}

	return res;
}

function secondToTime(sec)
{
	var m = sec / 60 ^ 0;
	var s = sec - m * 60 ^ 0;

	return (m ? m +' мин. ' : '') +(s < 10 ? '0' +s : s) +' сек.';
}

function addToLogFile(text)
{
	let nowDate = new Date();
	if (isDev)
	{
		console.log(text);
	}
	fs.appendFileSync('logFile.log', `[${nowDate.toLocaleString()}] ${text}\r\n`);
}

function addParsingInfo(text, value, name, padding = '')
{
	let div = document.createElement('div');
		div.innerHTML = text;
		div.className = 'parsingInfo--' +name +(padding == 'top' || padding == 'bottom' ? ' parsingInfo--padding-' +padding : '');

	let span = document.createElement('span');
		span.innerHTML = value;
		span.className = 'parsingInfo--value';

	div.append(span);
	parsingInfo.append(div);
	return span;
}