const { app, BrowserWindow } = require('electron');
app.commandLine.appendSwitch('js-flags', '--max-old-space-size=8192');

const isDev = require('electron-is-dev');
const path = require('path');

let mainWindow = null;

function createWindow()
{
	const iconPath = path.join(
		app.isPackaged ? app.getAppPath() : process.cwd(),
		'assets',
		'img',
		'icon.png'
	);

	mainWindow = new BrowserWindow(
	{
		height: 220,
		width: 300,
		frame: 0,
		resizable: 0,
		show: 0,
		webPreferences:
		{
			nodeIntegration: true,
			enableRemoteModule: true
		},
		title: 'Czar Of Scripts | Parser',
		icon: iconPath
	});

	mainWindow.setMenu(null);
	mainWindow.loadFile('./src/renderer/index.html');

	mainWindow.once('ready-to-show', () =>
	{
		mainWindow.show();

		if (isDev)
		{
			mainWindow.webContents.openDevTools();
		}
	});

	mainWindow.once('focus', () =>
	{
		mainWindow.flashFrame(0);
	});

	mainWindow.on("closed", () =>
	{
		mainWindow = null;
	});
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () =>
{
	if (process.platform !== 'darwin')
	{
		app.quit();
	}
});

app.on('activate', () =>
{
	if (BrowserWindow.getAllWindows().length === 0)
	{
		createWindow();
	}
});