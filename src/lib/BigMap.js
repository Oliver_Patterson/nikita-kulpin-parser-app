class BigMap
{
	constructor(...parameters)
	{
		this.maps = [new Map(...parameters)];
	}

	set(key, value)
	{
		const map = this.maps[this.maps.length - 1];

		if (map.size === Math.pow(2, 24))
		{
			this.maps.push(new Map());
			return this.set(key, value);
		}
		else
		{
			return map.set(key, value);
		}
	}

	has(key)
	{
		return this._mapForKey(key) !== undefined;
	}

	get(key)
	{
		return this._valueForKey(key);
	}

	clear()
	{
		for (let map of this.maps)
		{
			map.clear();
		}
	}
	_mapForKey(key)
	{
		for (let index = this.maps.length - 1; index >= 0; index--)
		{
			const map = this.maps[index];

			if (map.has(key))
			{
				return map;
			}
		}
	}

	_valueForKey(key)
	{
		for (let index = this.maps.length - 1; index >= 0; index--)
		{
			const map = this.maps[index];
			const value = map.get(key);

			if (value !== undefined)
			{
				return value;
			}
		}
	}
}
// BigMap.length = 0;

module.exports = BigMap;